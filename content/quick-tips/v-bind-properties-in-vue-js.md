+++
author = "calebporzio"
date = "2018-08-30T21:34:58+00:00"
draft = true
tags = ["vuejs"]
title = "v-bind properties in vue.js"
link = "https://twitter.com/calebporzio"
[[info]]
content = "📒📚 In @vuejs, instead of passing a bunch of object properties into a component as props, you can use v-bind as kind of a \"prop destructuring\""
image = "/uploads/DlyC8HZV4AAwLEs.jpg"
[[info]]
content = "And the \"prop\" component definition would stay the same and still work:"
image = "/uploads/DlyD3LaUwAAgNtQ.jpg"
+++
