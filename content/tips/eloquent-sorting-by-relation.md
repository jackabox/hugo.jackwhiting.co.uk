+++
author = ""
date = "2018-11-01T09:16:52+00:00"
draft = true
info = []
tags = ["laravel"]
title = "Eloquent: Sorting by Relation"
url = ""

+++
To sort by a relation, we need to do a manual join on the tables, order them by the field and then select the necessary response.

    Fair::join('fair_times', 'fair_times.fair_id', '=', 'fairs.id')
    	->orderBy('fair_times.opening', 'asc')
        ->select('fairs.*')