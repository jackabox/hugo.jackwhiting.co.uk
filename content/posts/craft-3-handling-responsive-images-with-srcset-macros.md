+++
author = ""
date = "2018-08-31T15:33:47+00:00"
draft = true
info = []
tags = ["craft3"]
title = "Craft 3: Handling responsive Images with srcset & macros"
url = ""

+++
# Setting Up Your Macro File

If you don't already have one, lets set up a macro file. We use a macro file to hold all of the [macros](https://twig.symfony.com/doc/2.x/tags/macro.html) our site may use. We can import this on each file and then call any of the macros contained. This is useful for a multitude of things, in this article we will concentrate on the macro for `srcset`.

Create a new file in your template directory called `macros.twig`.

Whenever you wish to use the macro file we'll need to import it into the twig file. To do this call the following at the top of your file.

    {% import './macros' as macros %}

# The `srcset` Macro

The following macro takes an image (asset) we pass through to it and outputs widths based on the parameters we set. For this example I've used **500, 800, 1200, 1600** as these were the transformations that I set up within the Craft admin.

We then work out if the output widths are less than or equal to the original image width and for each that is we add a new value to the src set array.

Finally we flatten the array and return the full list of srcs.

    {% macro srcset(image) %}
      {# setup #}
      {%- set outputWidths = [500, 800, 1200, 1600] -%}
      {%- set srcset = [] -%}
    
      {# if output width is smaller than or equal to the original image width #}
      {%- for outputWidth in outputWidths -%}
        {%- if outputWidth <= image.width -%}
          {%- set srcset = srcset | merge([image.url({ width: outputWidth }) ~ ' ' ~ outputWidth ~ 'w']) -%}
        {%- endif -%}
      {%- endfor -%}
    
      {# output srcset #}
      {{- srcset|join(', ') -}}
    {% endmacro %}

_Credit to_ [_Viget_](https://www.viget.com/articles/responsive-image-srcset-macro-for-craft/) _for the original macro._

To use the macro throughout your application, we'll need to call the srcset macro within our `<img>` tag. To do this, we'd do something along the lines of the following:

    {% set image = entry.image.one() %}
    
    <img src="{{ image.getUrl('large') }}"
         srcset="{{ macros.srcset(image) }}"
         sizes="100vw">