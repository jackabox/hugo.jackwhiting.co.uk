+++
date = "2018-08-31T15:38:35+00:00"
draft = true
tags = ["craft3"]
title = "Craft 3: Using axios and JavaScript to handle contact form submissions"

+++
This article presumes you'll know a little bit about how to set up a S3 Bucket, we'll go into brief details which should help you but there are a lot of articles around the internet that go through this.

**Prerequisites**

- Craft 3 installed
- An AWS (Amazon Web Services) account set up.

# Setting up S3 with Craft

Setting up an S3 bucket for use throughout your Craft application will require a couple of steps. If you've ever set up an S3 Bucket before, you may not need to do all of these steps - if any.

## Creating an IAM User

To access a bucket via the API you'll need an IAM user with the correct access. Generating a user will provide you with an API Key and an API Key Secret. You'll need these to set up the volumes in craft.

To set up a new user, we'll need to do the following:

1. Sign into your AWS Account.
2. Under the services dropdown, type IAM and select the "IAM" option that shows. This will take you to the IAM options.
3. In the sidebar select "Users".
4. In the top left click "Add User".
5. Enter a User Name.
6. Select Programmatic access as the "access type"
7. Click "Next: Permissions"
8. Click "Attach existing policies directly"
9. In the search field search for "AmazonS3". 
10. Toggle the "AmazonS3FullAccess" option which will allow for reading and writing to the buckets.
11. Click "Next: Review" to double check everything.
12. Click "Create User" if you are happy with everything.
13. Make a note of the "Access key ID" and "Secret access key" as you'll need these later.

## **Setting up a new bucket**

Setting up a new S3 bucket will only require a couple of steps if you wish to use the default settings. Any advanced settings are not going to be covered in this article.

1. In the AWS dashboard - Select services.
2. Type "S3" in the drop down and select the "S3" option.
3. Enter a bucket name (make it related to your project). 
4. Pick a region, make this the closest to where your traffic predominantly comes from.
5. Click "Next"
6. On the "Options" tab we'll just keep everything set to default as this is a good base point.
7. Click "Next"
8. Keep the top user enabled as this will allow you to manage the bucket via the admin.
9. Click "Next" 
10. Done! Your S3 bucket is all sorted. 

## Installing an AWS Plugin for Craft

In this article we'll use the official plugin provided by Craft, this can be found at [https://github.com/craftcms/aws-s3](https://github.com/craftcms/aws-s3).

You can install the plugin via the Craft admin, but we're going to do this via the terminal as it's a little simpler. To do so, we'll run the following:

    # go to the project directory
    cd /path/to/my-project
    
    # tell Composer to load the plugin
    composer require craftcms/aws-s3
    
    # tell Craft to install the plugin
    ./craft install/plugin aws-s3

# Connect Craft to S3

You now have to ways to set up the Craft system to work with your S3 volumes, you can manually define them per each volume or you can create an Asset configuration in the admin and then override the settings within `config/volumes.php`. We'll be doing the latter as it'll stop anyone accidentally overwriting them.

However, before Craft will know to overwrite the configuration, we'll actually need to create the asset. This feels a bit backwards but this happens because the data for available assets is stored within the database. Without creating one in the admin, the system is never going to know it exists.

## Create an Asset in the admin

If you already have an Asset configuration that exists, you can skip this step. If you don't we'll need to setup a new one.

1. Login to your admin panel.
2. Select Assets under the panel. 
3. Create a new volume.
4. Give it a name and handle.
5. Enable "Assets in this volume have public URLs".
6. Keep the folder as Local Folder - our config will override this anyway.

## Add our S3 settings to the .env file.

For added security, we don't want our S3 details to be tracked in our Git repository.

## Create your Volumes config

In your application folder create a new file of  `config/volumes.php`. Within this file, we can define an array of options which will overwrite the settings of any asset we define. 

If we have an asset called Users then our config would look 

# Assets with S3

Visit <url>/admin/settings/assets.

Create/edit the volume.

Set it as anything, in the admin.

Use the following example in volumes to overwrite any predefined / changed settings

    'users' => [
      'type' => 'craft\awss3\Volume',
      'hasUrls' => true,
      'url' => 'https://' . getenv('S3_BUCKET') . '.s3.amazonaws.com/',
      'keyId' => getenv('S3_API_KEY'),
      'secret' => getenv('S3_SECRET'),
      'bucket' => getenv('S3_BUCKET'),
      'region' => getenv('S3_REGION'),
    ],